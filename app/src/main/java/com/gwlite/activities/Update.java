package com.gwlite.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.gwlite.Parameters;
import com.gwlite.R;
import com.gwlite.adapters.FieldsAdapter;
import com.gwlite.entities.Field;
import com.gwlite.entities.Photo;
import com.gwlite.network.API;
import com.gwlite.util.GeoWorksParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Update extends AppCompatActivity {

    private Context mContext = this;
    private ActionBar mActionBar;
    private ProgressDialog mLoading;

    private RecyclerView mFieldsRecyclerView;
    private Button mUpdateButton;

    private FieldsAdapter mFieldsAdapter;
    private ArrayList<Field> mFields;

    private String mProjectId, mItemId;
    private int mPhotoCounter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        mFieldsRecyclerView = findViewById(R.id.fieldsRecyclerView);
        mUpdateButton = findViewById(R.id.updateButton);

        setupActionBar();

        mFieldsRecyclerView.setHasFixedSize(true);
        mFieldsRecyclerView.setLayoutManager(new LinearLayoutManager(mContext,
                LinearLayoutManager.VERTICAL, false));

        Bundle bundle = getIntent().getExtras();
        mProjectId = bundle.getString(Parameters.BUNDLE_PROJECT_ID);
        mItemId = bundle.getString(Parameters.BUNDLE_ITEM_ID);
        final boolean edit = bundle.getBoolean("edit");
        if (!edit) {
            mUpdateButton.setBackgroundResource(R.drawable.custom_disabled_button);
        }

        cleanFields();

        mUpdateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edit)
                    createJSON();
                else
                    showToast("No puede editarse un registro que no se encuentre cerca tu posición");
            }
        });
    }

    private void cleanFields() {
        if (mFields != null)
            mFields.clear();
        if (mFieldsRecyclerView.getAdapter() != null) {
            mFieldsRecyclerView.getAdapter().notifyDataSetChanged();
            mFieldsAdapter = null;
        }
        updateFields();
    }

    private void updateFields() {

        showProgressDialog();

        new API().getProjectFieldsUpdate(mItemId, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject result) {
                try {
                    JSONArray data = result.getJSONArray(Parameters.DATA);
                    readFields(data, result.getJSONArray(Parameters.PHOTOS));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                resetProgressDialog();
                showToast(error.toString());
            }
        });
    }

    private void readFields(JSONArray data, JSONArray photos) {
        try {
            mFields = new ArrayList<>();

            for (int i = 0; i < data.length(); i++) {
                Field field = GeoWorksParser.parseFieldUpdate(data.getJSONArray(i));
                if (field != null)
                    mFields.add(field);
            }

            for (int i = 0; i < photos.length(); i++) {
                JSONObject photo = photos.getJSONObject(i);
                mFields.add(new Field(6, photo.getString("name"), true, new Photo(photo.getString("image"))));
            }
            mPhotoCounter = photos.length();

            mFieldsAdapter = new FieldsAdapter(mContext, mFields, false);
            mFieldsRecyclerView.setAdapter(mFieldsAdapter);

            resetProgressDialog();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        mFieldsAdapter.onActivityResult(requestCode, resultCode, data);
    }

    private void createJSON() {

        showProgressDialog();

        final JSONObject properties = new JSONObject();
        for (Field field : mFields) {
            try {
                switch (field.getType()) {
                    case 1:
                    case 5:
                        properties.putOpt(field.getName(), field.getText().getValue());
                        break;
                    case 2:
                        properties.putOpt(field.getName(), field.getItems().getValue());
                        break;
                    case 3:
                        properties.putOpt(field.getName(), field.getDateItem().getValue());
                        break;
                    case 4:
                        properties.putOpt(field.getName(), field.getCheck().getValue());
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        new API().updateItem(mProjectId, mItemId, properties, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                resetProgressDialog();
                showToast("Registro modificado correctamente");
                setResult(Activity.RESULT_OK);
                ((Activity) mContext).finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                resetProgressDialog();
                System.out.println(error.toString());
                showToast(error.toString());
            }
        });
    }

    private void showToast(String message) {
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
    }

    private void resetProgressDialog() {
        if ((mLoading != null)) {
            if (mLoading.isShowing()) {
                mLoading.dismiss();
            }
        }
    }

    private void showProgressDialog() {
        mLoading = new ProgressDialog(mContext);
        mLoading.show();
        mLoading.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mLoading.setContentView(R.layout.custom_loading);
        mLoading.setCancelable(false);
        mLoading.setCanceledOnTouchOutside(false);
    }

    private void setupActionBar() {
        mActionBar = getSupportActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        //displaying custom ActionBar
        View mActionBarView = getLayoutInflater().inflate(R.layout.custom_action_bar, null);
        mActionBar.setCustomView(mActionBarView);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        mActionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.primary_color)));
        ImageButton backImageButton = mActionBarView.findViewById(R.id.actionImageButton);
        TextView titleTextView = mActionBarView.findViewById(R.id.titleTextView);
        ImageButton photoImageButton = mActionBarView.findViewById(R.id.action2ImageButton);

        titleTextView.setText("Modificar registro");

        backImageButton.setVisibility(ImageButton.VISIBLE);
        backImageButton.setImageResource(R.drawable.back);
        photoImageButton.setVisibility(ImageButton.VISIBLE);
        photoImageButton.setImageResource(R.drawable.camera);

        backImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCloseDialog();
            }
        });

        photoImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mFields != null) {
                    mPhotoCounter++;
                    mFields.add(new Field(6, "Foto " + mPhotoCounter, true, new Photo()));
                    if (mFieldsAdapter != null)
                        mFieldsAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        showCloseDialog();
    }

    private void showCloseDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("¿Deseas salir sin guardar los cambios?")
                .setPositiveButton("Sí, salir", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ((Activity) mContext).finish();
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        builder.show();
    }
}