package com.gwlite.util;

import com.gwlite.Parameters;
import com.gwlite.entities.Field;
import com.gwlite.entities.Item;
import com.gwlite.entities.Project;
import com.gwlite.entities.fields.Check;
import com.gwlite.entities.fields.DateItem;
import com.gwlite.entities.fields.Items;
import com.gwlite.entities.fields.ListItem;
import com.gwlite.entities.fields.Text;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class GeoWorksParser {

    public static Project parseProject(JSONObject project) {
        Project projectItem = null;
        try {

            String id = project.getString(Parameters.ID);

            JSONObject attributes = project.getJSONObject(Parameters.ATTRIBUTES);
            String name = attributes.getString(Parameters.NAME);

            projectItem = new Project(id, name);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return projectItem;
    }

    public static Field parseField(JSONObject field) {
        Field fieldItem = null;
        try {
            System.out.println(field.toString());
            JSONObject attributes = field.getJSONObject(Parameters.ATTRIBUTES);
            String name = attributes.getString(Parameters.NAME);

            if (!name.equals(Parameters.THE_GEOM)) {

                int type = attributes.getInt(Parameters.FIELD_TYPE);

                boolean required = attributes.getBoolean("required");
                switch (type) {
                    case 1:
                    case 5:
                        String regex = attributes.getString(Parameters.REGEX);
                        fieldItem = new Field(type, name, required, new Text(regex));
                        break;
                    case 2:
                        ArrayList<ListItem> itemsArray = new ArrayList<>();
                        JSONArray items = attributes.getJSONArray("items");
                        for (int x = 0; x < items.length(); x++) {
                            JSONObject listItem = items.getJSONObject(x);
                            int listItemId = listItem.getInt(Parameters.ID);
                            String listItemName = listItem.getString(Parameters.NAME);
                            itemsArray.add(new ListItem(listItemId, listItemName));
                        }
                        fieldItem = new Field(type, name, required, new Items(itemsArray));
                        break;
                    case 3:
                        fieldItem = new Field(type, name, required, new DateItem());
                        break;
                    case 4:
                        fieldItem = new Field(type, name, required, new Check());
                        break;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fieldItem;
    }

    public static Field parseFieldUpdate(JSONArray field) {
        Field fieldItem = null;
        try {

            JSONObject attributes = field.getJSONObject(0);
            JSONObject values = field.getJSONObject(3);

            String name = attributes.getString(Parameters.NAME);
            int type = attributes.getInt(Parameters.FIELD_TYPE);

            boolean required = attributes.getBoolean("required");
            switch (type) {
                case 1:
                case 5:
                    //String regex = attributes.getString(Parameters.REGEX);
                    fieldItem = new Field(type, name, required, new Text(""));
                    fieldItem.getText().setValue(values.getString("value"));
                    break;
                case 2:
                    ArrayList<ListItem> itemsArray = new ArrayList<>();
                    JSONArray items = field.getJSONObject(1).getJSONArray("items");
                    for (int x = 0; x < items.length(); x++) {
                        JSONObject listItem = items.getJSONObject(x);
                        int listItemId = listItem.getInt(Parameters.ID);
                        String listItemName = listItem.getString(Parameters.NAME);
                        itemsArray.add(new ListItem(listItemId, listItemName));
                    }
                    fieldItem = new Field(type, name, required, new Items(itemsArray));
                    fieldItem.getItems().setValue(values.getInt("value"));
                    break;
                case 3:
                    fieldItem = new Field(type, name, required, new DateItem());
                    fieldItem.getDateItem().setValue(values.getLong("value"));
                    break;
                case 4:
                    fieldItem = new Field(type, name, required, new Check());
                    fieldItem.getCheck().setValue(values.getBoolean("value"));
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return fieldItem;
    }

    public static Item parseItem(JSONObject item) {
        Item itemItem = null;
        try {
            String id = item.getString(Parameters.ID);

            JSONObject attributes = item.getJSONObject(Parameters.ATTRIBUTES);

            JSONArray geom = attributes.getJSONArray(Parameters.THE_GEOM);
            double latitude = geom.getDouble(1);
            double longitude = geom.getDouble(0);

            itemItem = new Item(id, latitude, longitude);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return itemItem;
    }

}