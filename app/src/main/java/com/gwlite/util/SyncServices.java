package com.gwlite.util;

import android.content.Context;
import android.util.Log;

import com.gwlite.AppController;
import com.gwlite.Parameters;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import de.greenrobot.dao.query.WhereCondition;
import model.Project;
import model.ProjectDao;

public class SyncServices {

    private Context mContext;

    public SyncServices(Context mContext) {
        this.mContext = mContext;
    }

    public int getAmountUnsyncedItems() {
        try {
            ProjectDao projectDao = AppController.getSession().getProjectDao();

            WhereCondition wc1 = new WhereCondition.PropertyCondition(ProjectDao.Properties.Uploaded,
                    " = " + 0);
            List<Project> query = projectDao.queryBuilder().where(wc1).listLazyUncached();

            return query.size();

        } catch (Exception e) {
            Log.d("SyncServices", "Error getAmountUnsyncedItems");
        }
        return 0;
    }

    public void saveItem(String projectId, String properties, String photos, double latitude, double longitude) {

        ProjectDao projectDao = AppController.getSession().getProjectDao();

        try {
            Project project = new Project();
            project.setProjectId(projectId);
            project.setProperties(properties);
            project.setPhotos(photos);
            project.setLatitude(latitude);
            project.setLongitude(longitude);
            project.setDate(System.currentTimeMillis());
            project.setUploaded(false);

            projectDao.insert(project);

        } catch (Exception e) {
            Log.d("SyncServices", "Error getUnsyncedItems");
        }
    }

    public JSONArray getUnsyncedItems() {
        try {
            ProjectDao projectDao = AppController.getSession().getProjectDao();

            WhereCondition wc1 = new WhereCondition.PropertyCondition(ProjectDao.Properties.Uploaded,
                    " = " + 0);
            List<Project> query = projectDao.queryBuilder().where(wc1).listLazyUncached();

            JSONArray arrayItems = new JSONArray();
            for (int i = 0; i < query.size(); i++) {
                Project project = query.get(i);
                try {
                    JSONObject item = new JSONObject();
                    item.put(Parameters.PROJECT_TYPE_ID, project.getProjectId());
                    item.put(Parameters.PROPERTIES, new JSONObject(project.getProperties()));
                    item.put(Parameters.PHOTOS, new JSONArray(project.getPhotos()));
                    item.put(Parameters.LATITUDE, project.getLongitude());
                    item.put(Parameters.LONGITUDE, project.getLongitude());
                    arrayItems.put(item);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return arrayItems;

        } catch (Exception e) {
            Log.d("SyncServices", "Error getUnsyncedItems");
        }
        return null;
    }

    public void deleteSyncedItems() {
        try {
            ProjectDao projectDao = AppController.getSession().getProjectDao();

            WhereCondition wc1 = new WhereCondition.PropertyCondition(ProjectDao.Properties.Uploaded,
                    " = " + 0);
            List<Project> query = projectDao.queryBuilder().where(wc1).listLazyUncached();

            for (int i = 0; i < query.size(); i++) {
                Project project = query.get(i);
                projectDao.delete(project);
            }

        } catch (Exception e) {
            Log.d("SyncServices", "Error deleteSyncedItems");
        }
    }

}