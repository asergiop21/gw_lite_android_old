package com.gwlite.adapters.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

import com.gwlite.R;

public class SpinnerViewHolder extends RecyclerView.ViewHolder {

    private TextView titleTextView;
    private Spinner spinner;

    public SpinnerViewHolder(View v) {
        super(v);
        titleTextView = v.findViewById(R.id.titleTextView);
        spinner = v.findViewById(R.id.spinner);
    }

    public TextView getTitleTextView() {
        return titleTextView;
    }

    public Spinner getSpinner() {
        return spinner;
    }
}