package com.gwlite;

import android.content.Context;
import android.content.SharedPreferences;

import com.gwlite.entities.Instance;
import com.gwlite.entities.User;

public class SharedPreferencesManager {

    private static SharedPreferencesManager instance = null;

    public String PREFERENCES = "com.geoworks";

    public String USER_ID = "userId";
    public String USER_EMAIL = "userEmail";

    public String INSTANCE_NAME = "instanceName";
    public String INSTANCE_URL = "instanceUrl";

    private SharedPreferences mSharedPreference;
    private SharedPreferences.Editor mEditor;

    public SharedPreferencesManager(Context context) {
        mSharedPreference = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
    }

    public static SharedPreferencesManager getInstance() {
        if (instance == null) {
            instance = new SharedPreferencesManager(AppController.getInstance());
        }
        return instance;
    }

    public void setUser(String userId, String userEmail) {
        mEditor = mSharedPreference.edit();
        mEditor.putString(USER_ID, userId);
        mEditor.putString(USER_EMAIL, userEmail);
        mEditor.commit();
    }

    public User getUser() {
        String userId = mSharedPreference.getString(USER_ID, "");
        String userEmail = mSharedPreference.getString(USER_EMAIL, "");
        User user = new User(userId, userEmail);
        return user;
    }

    public void setAppInstance(String instanceName, String instanceUrl) {
        mEditor = mSharedPreference.edit();
        mEditor.putString(INSTANCE_NAME, instanceName);
        mEditor.putString(INSTANCE_URL, instanceUrl);
        mEditor.commit();
    }

    public Instance getAppInstance() {
        String instanceName = mSharedPreference.getString(INSTANCE_NAME, "");
        String instanceUrl = mSharedPreference.getString(INSTANCE_URL, "");
        Instance instance = new Instance(instanceName, instanceUrl);
        return instance;
    }

}