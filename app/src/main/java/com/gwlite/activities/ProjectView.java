package com.gwlite.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.gwlite.Parameters;
import com.gwlite.R;
import com.gwlite.SharedPreferencesManager;
import com.gwlite.adapters.ProjectsAdapter;
import com.gwlite.entities.Item;
import com.gwlite.entities.Project;
import com.gwlite.network.API;
import com.gwlite.util.GeoWorksParser;
import com.gwlite.util.SyncServices;
import com.gwlite.util.rangebar.RangeBar;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.ViewObject;
import com.here.android.mpa.mapping.MapFragment;
import com.here.android.mpa.mapping.MapGesture;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.mapping.MapObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

import static com.gwlite.Parameters.BUNDLE_PROJECT_ID;
import static com.gwlite.Parameters.BUNDLE_PROJECT_NAME;

public class ProjectView extends AppCompatActivity implements OnEngineInitListener, EasyPermissions.PermissionCallbacks {

    private static final String TAG = "Map";
    private final int RESULT_STORAGE_PERMISSIONS = 100;
    private final int RESULT_LOCATION_PERMISSIONS = 101;

    private final int RESULT_UPDATE = 1;

    private Context mContext = this;
    private ActionBar mActionBar;
    private TextView mTitleTextView;

    private ProgressDialog mLoading;

    private DrawerLayout mDrawerLayout;
    private ListView mMenuListView;
    private MapFragment mMapFragment;
    private RangeBar<Integer> mRangeBar;
    private RelativeLayout mInfoRelativeLayout;
    private TextView mIdTextView;
    private FloatingActionButton mCreateFAB;

    private com.here.android.mpa.mapping.Map mMap;
    //private PositioningManager posManager;

    private ArrayList<Item> items;
    private List<MapObject> mMarkers;
    private Item mItemSelected;

    private String mProjectId;
    private MapMarker mPreviousMarker = null;
    private int mMarkerCounter;

    private com.here.android.mpa.common.Image mMarkerDotSelected;
    private com.here.android.mpa.common.Image mMarkerDot;

    private EditText mProjectEditText;
    private ProjectsAdapter mProjectsAdapter;
    private ArrayList<Project> mProjects;

    //Location
    private LocationRequest mLocationRequest;
    private LocationCallback mLocationCallback;
    private FusedLocationProviderClient mFusedLocationClient;
    private Location mCurrentPosition;

    private BroadcastReceiver mMenuReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            mProjectId = bundle.getString(BUNDLE_PROJECT_ID);
            String name = bundle.getString(BUNDLE_PROJECT_NAME);
            mTitleTextView.setText(name);
            mDrawerLayout.closeDrawers();
            cleanItems();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_view);

        mDrawerLayout = findViewById(R.id.drawerLayout);
        mMenuListView = findViewById(R.id.menuListView);
        mMapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.mapfragment);
        mRangeBar = findViewById(R.id.rangeBar);
        mInfoRelativeLayout = findViewById(R.id.infoRelativeLayouit);
        mIdTextView = findViewById(R.id.idTextView);
        mCreateFAB = findViewById(R.id.createFAB);

        setupActionBar();
        setupMenu();

        mRangeBar.setRangeValues(100, 10000, 100);
        mRangeBar.setSelectedMaxValue(100);

        mRangeBar.setOnRangeSeekBarChangeListener(new RangeBar.OnRangeSeekBarChangeListener<Integer>() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeBar<Integer> bar, Integer minValue, Integer maxValue) {
                mMap.removeMapObjects(mMarkers);
                mMarkerCounter = 0;
                if (mMarkerCounter < items.size()) {
                    loadMarkers();
                }
            }
        });

        mInfoRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mCurrentPosition != null) {
                    float distance = distance(mCurrentPosition.getLatitude(), mCurrentPosition.getLongitude(),
                            mItemSelected.getItemLatitude(), mItemSelected.getItemLongitude());

                    Intent intent = new Intent(mContext, Update.class);
                    intent.putExtra(BUNDLE_PROJECT_ID, mProjectId);
                    intent.putExtra(Parameters.BUNDLE_ITEM_ID, mItemSelected.getItemId());
                    if (distance < 250)
                        intent.putExtra("edit", true);
                    else
                        intent.putExtra("edit", false);
                    startActivityForResult(intent, RESULT_UPDATE);

                } else
                    showToast("La posicion actual del dispositivo no ha podido ser adquirida");

                if (mPreviousMarker != null)
                    mPreviousMarker.setIcon(mMarkerDot);

                mInfoRelativeLayout.setVisibility(RelativeLayout.GONE);
            }
        });

        mCreateFAB.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {
                if (mCurrentPosition != null) {
                    Intent intent = new Intent(mContext, Create.class);
                    intent.putExtra(BUNDLE_PROJECT_ID, mProjectId);
                    intent.putExtra(Parameters.LATITUDE, mCurrentPosition.getLatitude());
                    intent.putExtra(Parameters.LONGITUDE, mCurrentPosition.getLongitude());
                    startActivityForResult(intent, RESULT_UPDATE);
                } else
                    showToast("La posicion actual del dispositivo no ha podido ser adquirida");
            }
        });

        showStoragePermissions();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mMenuReceiver, new IntentFilter("menu"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mMenuReceiver);
    }

    @Override
    public void onEngineInitializationCompleted(Error error) {
        if (error == OnEngineInitListener.Error.NONE) {
            mMap = mMapFragment.getMap();
            showLocationPermissions();
        } else {
            Log.d(TAG, "ERROR:" + error.getDetails());
        }
    }

    public void onDestroy() {
        mMap = null;
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == RESULT_UPDATE) {
                cleanItems();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @AfterPermissionGranted(RESULT_STORAGE_PERMISSIONS)
    private void showStoragePermissions() {
        String[] perms = {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (EasyPermissions.hasPermissions(mContext, perms)) {
            setupMap();
            cleanItems();
        } else {
            EasyPermissions.requestPermissions(this, "Permitenos acceder a tu almacenamiento para poder desplegar el mapa",
                    RESULT_STORAGE_PERMISSIONS, perms);
        }
    }

    @AfterPermissionGranted(RESULT_LOCATION_PERMISSIONS)
    private void showLocationPermissions() {
        String[] perms = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
        if (EasyPermissions.hasPermissions(mContext, perms)) {
            requestLocation();
        } else {
            EasyPermissions.requestPermissions(this,
                    "Permitenos acceder a tu ubicación para poder mostrarte el mejor contenido posible", RESULT_LOCATION_PERMISSIONS,
                    perms);
        }
    }

    @SuppressLint("MissingPermission")
    public void requestLocation() {

        stopLocation();

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult result) {
                super.onLocationResult(result);
                Location location = result.getLocations().get(0);
                if (location != null) {
                    if (mCurrentPosition == null) {
                        GeoCoordinate coordinate = new GeoCoordinate(location.getLatitude(), location.getLongitude());
                        mMap.setCenter(coordinate, com.here.android.mpa.mapping.Map.Animation.LINEAR);
                        mMap.setZoomLevel(8);
                    }
                    mCurrentPosition = location;
                }
            }

            @Override
            public void onLocationAvailability(LocationAvailability availability) {
            }
        };
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
    }

    private void stopLocation() {
        if (mFusedLocationClient != null && mLocationCallback != null) {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
            mFusedLocationClient = null;
            mLocationCallback = null;
        }
    }

    private void setupMap() {
        mMapFragment.init(this);
    }

    private void setupGestures() {
        MapGesture.OnGestureListener listener = new MapGesture.OnGestureListener.OnGestureListenerAdapter() {
            @Override
            public boolean onMapObjectsSelected(List<ViewObject> objects) {
                for (ViewObject viewObj : objects) {
                    if (viewObj.getBaseType() == ViewObject.Type.USER_OBJECT) {
                        if (((MapObject) viewObj).getType() == MapObject.Type.MARKER) {

                            if (mPreviousMarker != null)
                                mPreviousMarker.setIcon(mMarkerDot);

                            MapMarker marker = (MapMarker) viewObj;
                            marker.setIcon(mMarkerDotSelected);

                            mPreviousMarker = marker;

                            try {
                                mItemSelected = items.get(Integer.parseInt(marker.getTitle()));

                                mIdTextView.setText("Marcador " + marker.getTitle());
                                mInfoRelativeLayout.setVisibility(RelativeLayout.VISIBLE);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                return false;
            }
        };
        mMapFragment.getMapGesture().addOnGestureListener(listener);
    }

    private void setupMenu() {
        final View header = getLayoutInflater().inflate(R.layout.item_menu_header, null);
        mProjectEditText = header.findViewById(R.id.projectEditText);

        mMenuListView.addHeaderView(header);

        cleanProjects();
    }

    private void cleanProjects() {
        //mErrorRelativeLayout.setVisibility(RelativeLayout.GONE);
        if (mProjects != null)
            mProjects.clear();
        if (mProjectsAdapter != null) {
            mProjectsAdapter.notifyDataSetChanged();
            mProjectsAdapter = null;
        }
        updateProjects();
    }

    private void updateProjects() {

        showProgressDialog();

        new API().getProjects(new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject result) {
                try {
                    JSONArray data = result.getJSONArray(Parameters.DATA);
                    readProjects(data);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                resetProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error.toString());
                resetProgressDialog();
                showErrorAnimation();
            }
        });
    }

    private void showErrorAnimation() {
        //mErrorRelativeLayout.setVisibility(RelativeLayout.VISIBLE);
    }

    private void readProjects(JSONArray data) {
        try {
            mProjects = new ArrayList<>();

            for (int i = 0; i < data.length(); i++) {
                Project project = GeoWorksParser.parseProject(data.getJSONObject(i));
                if (project != null)
                    mProjects.add(project);
            }

            mProjectsAdapter = new ProjectsAdapter(mContext, mProjects);
            mMenuListView.setAdapter(mProjectsAdapter);

            mMenuListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Project project = mProjects.get(i - 1);

                    Intent intent = new Intent("menu");
                    intent.putExtra(BUNDLE_PROJECT_ID, project.getProjectId());
                    intent.putExtra(BUNDLE_PROJECT_NAME, project.getProjectName());
                    sendBroadcast(intent);
                }
            });

            mProjectEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    mProjectsAdapter.filter(charSequence.toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cleanItems() {
        if (mMap != null && mMarkers != null)
            mMap.removeMapObjects(mMarkers);
        if (items != null)
            items.clear();
        if (!mProjectId.equals(""))
            updateItems();
    }

    private void updateItems() {

        showProgressDialog();
        System.out.println(System.currentTimeMillis());

        new API().getProjectItems(mProjectId, new Response.Listener<JSONObject>() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onResponse(JSONObject result) {
                try {
                    JSONArray data = result.getJSONArray(Parameters.DATA);
                    readItems(data);
                    mRangeBar.setVisibility(RangeBar.VISIBLE);
                    mCreateFAB.setVisibility(FloatingActionButton.VISIBLE);
                } catch (Exception e) {
                    e.printStackTrace();
                    mCreateFAB.setVisibility(FloatingActionButton.GONE);
                }
                resetProgressDialog();

                if (mMarkerCounter < items.size()) {
                    loadMarkers();
                }

            }
        }, new Response.ErrorListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onErrorResponse(VolleyError error) {
                resetProgressDialog();
                showToast(error.toString());
                mCreateFAB.setVisibility(FloatingActionButton.GONE);
            }
        });
    }

    private void readItems(JSONArray data) {
        try {

            try {
                mMarkerDot = new com.here.android.mpa.common.Image();
                mMarkerDotSelected = new com.here.android.mpa.common.Image();
                mMarkerDot.setImageResource(R.drawable.dot);
                mMarkerDotSelected.setImageResource(R.drawable.dot_selected);
            } catch (Exception e) {
                e.printStackTrace();
            }

            items = new ArrayList<>();
            mMarkers = new ArrayList<>();

            for (int i = 0; i < data.length(); i++) {
                Item item = GeoWorksParser.parseItem(data.getJSONObject(i));
                if (item != null) {
                    items.add(item);
                }
            }

            if (items.size() > 0) {
                /*GeoCoordinate coordinate = new GeoCoordinate(items.get(items.size() - 1).getItemLatitude(),
                        items.get(items.size() - 1).getItemLongitude());
                mMap.setCenter(coordinate, com.here.android.mpa.mapping.Map.Animation.LINEAR);
                mMap.setZoomLevel(8);*/
                setupGestures();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadMarkers() {
        if (mCurrentPosition != null) {
            Item item = items.get(mMarkerCounter);
            float distance = distance(mCurrentPosition.getLatitude(), mCurrentPosition.getLongitude(),
                    item.getItemLatitude(), item.getItemLongitude());
            if (distance < mRangeBar.getSelectedMaxValue()) {
                GeoCoordinate coordinate = new GeoCoordinate(item.getItemLatitude(), item.getItemLongitude());
                MapMarker marker = new MapMarker(coordinate, mMarkerDot);
                mMarkers.add(marker);
                marker.setTitle(String.valueOf(mMarkerCounter));
                mMap.addMapObject(marker);
            }
            mMarkerCounter++;
            if (mMarkerCounter < items.size()) {
                loadMarkers();
            }
        } else
            showToast("La posicion actual del dispositivo no ha podido ser adquirida");
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        if (requestCode == RESULT_STORAGE_PERMISSIONS) {
            showToast("Permitenos acceder a tu almacenamiento para poder desplegar el mapa");
        }

        if (requestCode == RESULT_LOCATION_PERMISSIONS) {
            showToast("Permitenos acceder a tu ubicación para poder mostrarte el mejor contenido posible");
        }
    }

    private void showToast(String message) {
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
    }

    private void resetProgressDialog() {
        if ((mLoading != null)) {
            if (mLoading.isShowing()) {
                mLoading.dismiss();
            }
        }
    }

    private void showProgressDialog() {
        mLoading = new ProgressDialog(mContext);
        mLoading.show();
        mLoading.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mLoading.setContentView(R.layout.custom_loading);
        mLoading.setCancelable(false);
        mLoading.setCanceledOnTouchOutside(false);
    }

    @SuppressLint("RestrictedApi")
    private void setupActionBar() {
        mActionBar = getSupportActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        //displaying custom ActionBar
        View mActionBarView = getLayoutInflater().inflate(R.layout.custom_action_bar, null);
        mActionBar.setCustomView(mActionBarView);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        mActionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.primary_color)));
        ImageButton menuImageButton = mActionBarView.findViewById(R.id.actionImageButton);
        ImageButton syncImageButton = mActionBarView.findViewById(R.id.action2ImageButton);
        ImageButton logoutImageButton = mActionBarView.findViewById(R.id.action3ImageButton);

        mTitleTextView = mActionBarView.findViewById(R.id.titleTextView);

        mProjectId = "";
        mTitleTextView.setText("Seleccionar proyecto");
        mCreateFAB.setVisibility(FloatingActionButton.GONE);

        menuImageButton.setVisibility(ImageButton.VISIBLE);
        menuImageButton.setImageResource(R.drawable.menu);

        menuImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mDrawerLayout.isDrawerOpen(GravityCompat.START))
                    mDrawerLayout.closeDrawers();
                else
                    mDrawerLayout.openDrawer(mMenuListView);
            }
        });

        if (!getSync()) {
            syncImageButton.setVisibility(ImageButton.VISIBLE);
            syncImageButton.setImageResource(R.drawable.sync_off);
        } else
            syncImageButton.setVisibility(ImageButton.GONE);

        syncImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                syncItems();
            }
        });

        logoutImageButton.setVisibility(ImageButton.VISIBLE);
        logoutImageButton.setImageResource(R.drawable.logout);

        logoutImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearPreferences();
            }
        });
    }

    private boolean getSync() {
        int items = new SyncServices(mContext).getAmountUnsyncedItems();
        if (items > 0)
            return false;

        return true;
    }

    private void syncItems() {

        JSONArray arrayItems = new SyncServices(mContext).getUnsyncedItems();

        new API().createItem(arrayItems, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                resetProgressDialog();
                showToast("Registros sincronizados correctamente");
                new SyncServices(mContext).deleteSyncedItems();
                setupActionBar();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                resetProgressDialog();
                System.out.println(error.toString());
                showToast(error.toString());
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START))
            mDrawerLayout.closeDrawers();
        else
            super.onBackPressed();
    }

    public float distance(double lat_a, double lng_a, double lat_b, double lng_b) {
        double earthRadius = 3958.75;
        double latDiff = Math.toRadians(lat_b - lat_a);
        double lngDiff = Math.toRadians(lng_b - lng_a);
        double a = Math.sin(latDiff / 2) * Math.sin(latDiff / 2) +
                Math.cos(Math.toRadians(lat_a)) * Math.cos(Math.toRadians(lat_b)) *
                        Math.sin(lngDiff / 2) * Math.sin(lngDiff / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = earthRadius * c;

        int meterConversion = 1609;

        return (float) (distance * meterConversion);
    }


    private void clearPreferences() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage("¿Deseas salir de la sesión actual?")
                .setPositiveButton("Cerrar sesión", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        SharedPreferences settings = getSharedPreferences(SharedPreferencesManager.getInstance().PREFERENCES, Context.MODE_PRIVATE);
                        settings.edit().clear().commit();

                        mLoading.dismiss();
                        finish();
                        Intent splash = new Intent(mContext, Login.class);
                        startActivity(splash);
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        builder.show();
    }

}