package com.gwlite.adapters.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.gwlite.R;

public class DateViewHolder extends RecyclerView.ViewHolder {

    private TextView titleTextView;
    private TextView dateTextView;

    public DateViewHolder(View v) {
        super(v);
        titleTextView = v.findViewById(R.id.titleTextView);
        dateTextView = v.findViewById(R.id.dateTextView);
    }

    public TextView getTitleTextView() {
        return titleTextView;
    }

    public TextView getDateTextView() {
        return dateTextView;
    }
}