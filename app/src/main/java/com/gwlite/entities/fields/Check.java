package com.gwlite.entities.fields;

import java.io.Serializable;

public class Check implements Serializable {

    protected boolean value;

    public Check() {

    }

    public boolean getValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

}
