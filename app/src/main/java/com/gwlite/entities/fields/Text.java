package com.gwlite.entities.fields;

import java.io.Serializable;

public class Text implements Serializable {

    protected String regex;
    protected String value;

    public Text(String regex) {
        this.regex = regex;
    }

    public String getRegex() {
        return regex;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
