package com.gwlite.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.gwlite.Parameters;
import com.gwlite.R;
import com.gwlite.SharedPreferencesManager;
import com.gwlite.adapters.CompaniesAdapter;
import com.gwlite.entities.Instance;
import com.gwlite.network.API;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class Login extends AppCompatActivity implements TextWatcher {

    private Context mContext = this;
    private ProgressDialog mLoading;

    private LinearLayout mLoginLinearLayout, mCompaniesLinearLayout;
    private EditText mEmailEditText, mPasswordEditText;
    private Button mLoginButton;
    private RecyclerView mCompaniesRecyclerView;

    private String mEmail, mPassword;

    @Override
    protected void onStart() {
        super.onStart();

        if (!SharedPreferencesManager.getInstance().getUser().getUserId().equals("")) {
            ((Activity) mContext).finish();
            Intent main = new Intent(mContext, ProjectView.class);
            startActivity(main);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mLoginLinearLayout = findViewById(R.id.loginLinearLayout);
        mEmailEditText = findViewById(R.id.emailEditText);
        mPasswordEditText = findViewById(R.id.passwordEditText);
        mLoginButton = findViewById(R.id.loginButton);
        mCompaniesLinearLayout = findViewById(R.id.companiesLinearLayout);
        mCompaniesRecyclerView = findViewById(R.id.companiesRecyclerView);

        mCompaniesRecyclerView.setHasFixedSize(true);
        mCompaniesRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));

        mEmailEditText.addTextChangedListener(this);
        mPasswordEditText.addTextChangedListener(this);

        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateFields(true)) {
                    login();
                }
            }
        });
    }

    private void login() {

        showProgressDialog();

        new API().login(mEmail, mPassword, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject data = response.getJSONObject(Parameters.DATA);
                    final String userId = data.getString(Parameters.USER_ID);

                    mLoginLinearLayout.setVisibility(LinearLayout.GONE);
                    mLoginButton.setVisibility(Button.GONE);
                    mCompaniesLinearLayout.setVisibility(LinearLayout.VISIBLE);

                    ArrayList<Instance> instances = new ArrayList<>();
                    JSONArray companies = response.getJSONArray("companies");
                    for (int i = 0; i < companies.length(); i++) {
                        String name = companies.getJSONObject(i).getString("name_company");
                        String url = companies.getJSONObject(i).getString("url");
                        instances.add(new Instance(name, url));
                    }
                    CompaniesAdapter companiesAdapter = new CompaniesAdapter(mContext, instances, new CompaniesAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(Instance item, int position) {
                            SharedPreferencesManager.getInstance().setUser(userId, mEmail);
                            SharedPreferencesManager.getInstance().setAppInstance(item.getInstanceName(), item.getInstanceUrl());
                            ((Activity) mContext).finish();
                            Intent main = new Intent(mContext, ProjectView.class);
                            startActivity(main);
                        }
                    });
                    mCompaniesRecyclerView.setAdapter(companiesAdapter);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                resetProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                resetProgressDialog();
                if (error.getClass().equals(AuthFailureError.class))
                    showToast("Email o contraseña no correctos");
                else
                    showToast(error.toString());
            }
        });
    }

    private boolean validateFields(boolean focus) {

        mEmail = mEmailEditText.getText().toString();
        if (!mEmail.matches("[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\\.+[a-z].*[a-z]*")) {
            mLoginButton.setBackground(getResources().getDrawable(R.drawable.custom_disabled_button));
            if (focus) {
                showToast("Email invalido");
                mEmailEditText.requestFocus();
            }
            return false;
        }

        mPassword = mPasswordEditText.getText().toString();
        if (!mPassword.matches("[^#]{3,30}")) {
            mLoginButton.setBackground(getResources().getDrawable(R.drawable.custom_disabled_button));
            if (focus) {
                showToast("Password invalido");
                mPasswordEditText.requestFocus();
            }
            return false;
        }

        mLoginButton.setBackground(getResources().getDrawable(R.drawable.custom_button));
        return true;

    }

    private void showToast(String message) {
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
    }

    private void resetProgressDialog() {
        if ((mLoading != null)) {
            if (mLoading.isShowing()) {
                mLoading.dismiss();
            }
        }
    }

    private void showProgressDialog() {
        mLoading = new ProgressDialog(mContext);
        mLoading.show();
        mLoading.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mLoading.setContentView(R.layout.custom_loading);
        mLoading.setCancelable(false);
        mLoading.setCanceledOnTouchOutside(false);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        validateFields(false);
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
