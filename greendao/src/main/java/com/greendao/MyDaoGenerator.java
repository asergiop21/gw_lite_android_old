package com.greendao;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class MyDaoGenerator {

    public static void main(String args[]) throws Exception {

        Schema schema = new Schema(3, "model");

        Entity project = schema.addEntity("Project");
        project.addIdProperty();
        project.addStringProperty("projectId");
        project.addStringProperty("properties");
        project.addStringProperty("photos");
        project.addDoubleProperty("latitude");
        project.addDoubleProperty("longitude");
        project.addLongProperty("date");
        project.addBooleanProperty("uploaded");

        new DaoGenerator().generateAll(schema, args[0]);

    }
}