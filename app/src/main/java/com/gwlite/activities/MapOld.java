package com.gwlite.activities;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.gwlite.R;
import com.gwlite.adapters.PlacesAutoCompleteAdapter;
import com.here.android.mpa.common.GeoPosition;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.PositioningManager;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapFragment;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.routing.RouteManager;
import com.here.android.mpa.routing.RouteOptions;
import com.here.android.mpa.routing.RoutePlan;
import com.here.android.mpa.routing.RouteResult;
import com.here.android.mpa.search.AutoSuggest;
import com.here.android.mpa.search.AutoSuggestPlace;

import java.lang.ref.WeakReference;
import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class MapOld extends AppCompatActivity implements OnEngineInitListener, EasyPermissions.PermissionCallbacks {

    private static final String TAG = "MapOld";
    private final int RESULT_STORAGE_PERMISSIONS = 100;
    private final int RESULT_LOCATION_PERMISSIONS = 101;

    private Context mContext = this;
    private ProgressDialog mLoading;

    private AutoCompleteTextView mLocationAutoComplete;
    private MapFragment mMapFragment;

    private Map mMap;
    private PositioningManager posManager;
    private PlacesAutoCompleteAdapter mPlacesAutoCompleteAdapter;

    private GeoPosition mCurrentPosition;
    private MapRoute mMapRoute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_old);

        mLocationAutoComplete = findViewById(R.id.locationAutoCompleteTextView);
        mMapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.mapfragment);

        mPlacesAutoCompleteAdapter = new PlacesAutoCompleteAdapter(mContext, R.id.placeTextView);
        mLocationAutoComplete.setAdapter(mPlacesAutoCompleteAdapter);

        showStoragePermissions();

        mLocationAutoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                hideKeyboard((Activity) mContext);
                getRoute(mPlacesAutoCompleteAdapter.getItem(i));
                mLocationAutoComplete.setText("");
            }
        });
    }

    @Override
    public void onEngineInitializationCompleted(Error error) {
        if (error == OnEngineInitListener.Error.NONE) {
            mMap = mMapFragment.getMap();
            mMap.getPositionIndicator().setVisible(true);

            posManager = PositioningManager.getInstance();
            posManager.addListener(new WeakReference<>(positionListener));

            showLocationPermissions();
        } else {
            Log.d(TAG, "ERROR:" + error.getDetails());
        }
    }

    private PositioningManager.OnPositionChangedListener positionListener = new PositioningManager.OnPositionChangedListener() {
        public void onPositionUpdated(PositioningManager.LocationMethod method, GeoPosition position, boolean isMapMatched) {
            if (mCurrentPosition == null) {
                mMap.setCenter(position.getCoordinate(), Map.Animation.NONE);
                mMap.setZoomLevel(16.0);
                resetProgressDialog();
            }
            mCurrentPosition = position;
        }

        public void onPositionFixChanged(PositioningManager.LocationMethod method, PositioningManager.LocationStatus status) {
        }
    };

    private void getRoute(AutoSuggest autoSuggest) {

        showProgressDialog("Cargando ruta...");

        try {

            if (mMapRoute != null)
                mMap.removeMapObject(mMapRoute);

            final AutoSuggestPlace autoSuggestPlace = (AutoSuggestPlace) autoSuggest;

            RouteManager rm = new RouteManager();

            RoutePlan routePlan = new RoutePlan();
            routePlan.addWaypoint(mCurrentPosition.getCoordinate());
            routePlan.addWaypoint(autoSuggestPlace.getPosition());

            RouteOptions routeOptions = new RouteOptions();
            routeOptions.setTransportMode(RouteOptions.TransportMode.CAR);
            routeOptions.setRouteType(RouteOptions.Type.FASTEST);

            routePlan.setRouteOptions(routeOptions);

            rm.calculateRoute(routePlan, new RouteManager.Listener() {
                @Override
                public void onProgress(int i) {

                }

                @Override
                public void onCalculateRouteFinished(RouteManager.Error error, List<RouteResult> list) {
                    if (error == RouteManager.Error.NONE) {

                        mMapRoute = new MapRoute(list.get(0).getRoute());
                        mMap.addMapObject(mMapRoute);

                    } else {
                        Log.d(TAG, "ERROR:" + error.getDeclaringClass());
                    }
                    resetProgressDialog();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            resetProgressDialog();
        }
    }

    /*@Override
    protected void onResume() {
        super.onResume();
        if (posManager != null) {
            showLocationPermissions();
        }
    }*/

    public void onPause() {
        if (posManager != null) {
            posManager.stop();
        }
        super.onPause();
    }

    public void onDestroy() {
        if (posManager != null) {
            posManager.removeListener(positionListener);
        }
        mMap = null;
        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @AfterPermissionGranted(RESULT_STORAGE_PERMISSIONS)
    private void showStoragePermissions() {
        String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (EasyPermissions.hasPermissions(this, perms)) {
            mMapFragment.init(this);
        } else {
            EasyPermissions.requestPermissions(this, "Permitenos acceder a tu almacenamiento para poder desplegar el mapa",
                    RESULT_STORAGE_PERMISSIONS, perms);
        }
    }

    @AfterPermissionGranted(RESULT_LOCATION_PERMISSIONS)
    private void showLocationPermissions() {
        String[] perms = {Manifest.permission.ACCESS_FINE_LOCATION};
        if (EasyPermissions.hasPermissions(this, perms)) {
            showProgressDialog("Localizando...");
            posManager.start(PositioningManager.LocationMethod.GPS_NETWORK);
        } else {
            EasyPermissions.requestPermissions(this,
                    "Permitenos acceder a tu ubicación para poder mostrarte el mejor contenido posible", RESULT_LOCATION_PERMISSIONS,
                    perms);
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        if (requestCode == RESULT_STORAGE_PERMISSIONS) {
            showToast("Permitenos acceder a tu almacenamiento para poder desplegar el mapa");
        }

        if (requestCode == RESULT_LOCATION_PERMISSIONS) {
            showToast("Permitenos acceder a tu ubicación para poder mostrarte el mejor contenido posible");
        }
    }

    private void showToast(String message) {
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
    }

    private void resetProgressDialog() {
        if ((mLoading != null)) {
            if (mLoading.isShowing()) {
                mLoading.dismiss();
            }
        }
    }

    private void showProgressDialog(String text) {
        mLoading = new ProgressDialog(mContext);
        mLoading.show();
        mLoading.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mLoading.setContentView(R.layout.custom_loading);
        TextView message = mLoading.findViewById(R.id.messageTextView);
        message.setText(text);
        mLoading.setCancelable(false);
        mLoading.setCanceledOnTouchOutside(false);
    }

    private void hideKeyboard(Activity activity) {
        if (activity != null && activity.getWindow() != null && activity.getWindow().getDecorView() != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
        }
    }
}