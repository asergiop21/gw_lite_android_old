package com.gwlite.entities;

public class Project {

    protected String projectId;
    protected String projectName;
    protected long id;

    public Project(String projectId, String projectName) {
        this.projectId = projectId;
        this.projectName = projectName;
    }

    public String getProjectId() {
        return projectId;
    }

    public String getProjectName() {
        return projectName;
    }

    public long getId() {
        return id;
    }
}
