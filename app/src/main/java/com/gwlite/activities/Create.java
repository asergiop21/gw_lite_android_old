package com.gwlite.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.gwlite.AppController;
import com.gwlite.Parameters;
import com.gwlite.R;
import com.gwlite.adapters.FieldsParentAdapter;
import com.gwlite.entities.Field;
import com.gwlite.entities.FieldParent;
import com.gwlite.network.API;
import com.gwlite.util.GeoWorksParser;
import com.gwlite.util.SyncServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Create extends AppCompatActivity {

    private Context mContext = this;
    private ActionBar mActionBar;
    private ProgressDialog mLoading;

    private RecyclerView mFieldsRecyclerView;
    private Button mCreateButton;

    private FieldsParentAdapter mFieldsParentAdapter;
    private ArrayList<FieldParent> mFieldsParent;

    private String mProjectId;
    private double mLatitude, mLongitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);

        mFieldsRecyclerView = findViewById(R.id.fieldsRecyclerView);
        mCreateButton = findViewById(R.id.updateButton);

        setupActionBar();

        mFieldsRecyclerView.setHasFixedSize(true);
        mFieldsRecyclerView.setLayoutManager(new LinearLayoutManager(mContext,
                LinearLayoutManager.VERTICAL, false));

        Bundle bundle = getIntent().getExtras();
        mProjectId = bundle.getString(Parameters.BUNDLE_PROJECT_ID);
        mLatitude = bundle.getDouble(Parameters.LATITUDE);
        mLongitude = bundle.getDouble(Parameters.LONGITUDE);

        cleanFields();

        mCreateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createJSON();
            }
        });
    }

    private void cleanFields() {
        if (mFieldsParent != null)
            mFieldsParent.clear();
        if (mFieldsRecyclerView.getAdapter() != null) {
            mFieldsRecyclerView.getAdapter().notifyDataSetChanged();
            mFieldsParentAdapter = null;
        }
        updateFields();
    }

    private void updateFields() {

        showProgressDialog();

        new API().getProjectFields(mProjectId, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject result) {
                try {
                    JSONArray data = result.getJSONArray(Parameters.DATA);
                    readFields(data);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                resetProgressDialog();
                showToast(error.toString());
            }
        });
    }

    private void readFields(JSONArray data) {
        try {
            mFieldsParent = new ArrayList<>();
            ArrayList<Field> fields = new ArrayList<>();
            for (int i = 0; i < data.length(); i++) {
                Field field = GeoWorksParser.parseField(data.getJSONObject(i));
                if (field != null)
                    fields.add(field);
            }
            mFieldsParent.add(new FieldParent("Formulario: " + (mFieldsParent.size() + 1), fields));
            mFieldsParent.add(new FieldParent("Formulario: " + (mFieldsParent.size() + 1), fields));

            mFieldsParentAdapter = new FieldsParentAdapter(mContext, mFieldsParent, true);
            mFieldsRecyclerView.setAdapter(mFieldsParentAdapter);

            resetProgressDialog();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        mFieldsParentAdapter.onActivityResult(requestCode, resultCode, data);
    }

    private void createJSON() {

        showProgressDialog();

        final JSONArray array = new JSONArray();
        for (FieldParent fieldParent : mFieldsParent) {
            JSONObject properties = new JSONObject();
            for (Field field : fieldParent.getFields()) {
                try {
                    switch (field.getType()) {
                        case 1:
                        case 5:
                            properties.putOpt(field.getName(), field.getText().getValue());
                            break;
                        case 2:
                            properties.putOpt(field.getName(), field.getItems().getValue());
                            break;
                        case 3:
                            properties.putOpt(field.getName(), field.getDateItem().getValue());
                            break;
                        case 4:
                            properties.putOpt(field.getName(), field.getCheck().getValue());
                            break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            array.put(properties);
        }

        final JSONArray arrayPhotos = new JSONArray();
        for (FieldParent fieldParent : mFieldsParent) {
            final JSONArray photos_attributes = new JSONArray();
            for (Field field : fieldParent.getFields()) {
                try {
                    switch (field.getType()) {
                        case 6:
                            JSONObject photo = new JSONObject();
                            photo.putOpt("name", field.getName());
                            photo.putOpt("image", field.getPhoto().getValue());
                            photos_attributes.put(photo);
                            break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        JSONArray arrayItems = new JSONArray();
        try {
            JSONObject item = new JSONObject();
            item.put(Parameters.PROJECT_TYPE_ID, mProjectId);
            item.put(Parameters.PROPERTIES, array);
            item.put(Parameters.PHOTOS, arrayPhotos);
            item.put(Parameters.LATITUDE, mLatitude);
            item.put(Parameters.LONGITUDE, mLongitude);
            arrayItems.put(item);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (AppController.hasConnection()) {

            new API().createItem(arrayItems, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    resetProgressDialog();
                    showToast("Registro creado correctamente");
                    setResult(Activity.RESULT_OK);
                    ((Activity) mContext).finish();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    System.out.println(error.toString());

                    new SyncServices(mContext).saveItem(mProjectId, array.toString(), arrayPhotos.toString()
                            , mLatitude, mLongitude);
                    resetProgressDialog();
                    showToast("Registro se sincronizara luego");
                    ((Activity) mContext).finish();
                }
            });

        } else {
            new SyncServices(mContext).saveItem(mProjectId, array.toString(), arrayPhotos.toString()
                    , mLatitude, mLongitude);
            resetProgressDialog();
            showToast("Registro se sincronizara luego");
            ((Activity) mContext).finish();
        }

    }

    private void showToast(String message) {
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
    }

    private void resetProgressDialog() {
        if ((mLoading != null)) {
            if (mLoading.isShowing()) {
                mLoading.dismiss();
            }
        }
    }

    private void showProgressDialog() {
        mLoading = new ProgressDialog(mContext);
        mLoading.show();
        mLoading.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mLoading.setContentView(R.layout.custom_loading);
        mLoading.setCancelable(false);
        mLoading.setCanceledOnTouchOutside(false);
    }

    private void setupActionBar() {
        mActionBar = getSupportActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        //displaying custom ActionBar
        View mActionBarView = getLayoutInflater().inflate(R.layout.custom_action_bar, null);
        mActionBar.setCustomView(mActionBarView);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        mActionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.primary_color)));
        ImageButton backImageButton = mActionBarView.findViewById(R.id.actionImageButton);
        TextView titleTextView = mActionBarView.findViewById(R.id.titleTextView);

        titleTextView.setText("Crear nuevo registro");

        backImageButton.setVisibility(ImageButton.VISIBLE);
        backImageButton.setImageResource(R.drawable.back);

        backImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCloseDialog();
            }
        });
    }

    @Override
    public void onBackPressed() {
        showCloseDialog();
    }

    private void showCloseDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("¿Deseas salir sin crear el nuevo registro?")
                .setPositiveButton("Sí, salir", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ((Activity) mContext).finish();
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        builder.show();
    }
}