package com.gwlite.entities;

public class Instance {

    protected String instanceName;
    protected String instanceUrl;
    protected long id;

    public Instance(String instanceName, String instanceUrl) {
        this.instanceName = instanceName;
        this.instanceUrl = instanceUrl;
    }

    public String getInstanceName() {
        return instanceName;
    }

    public String getInstanceUrl() {
        return instanceUrl;
    }

    public long getId() {
        return id;
    }
}
