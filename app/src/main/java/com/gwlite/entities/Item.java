package com.gwlite.entities;

import java.util.ArrayList;

public class Item {

    protected String itemId;
    protected double itemLatitude;
    protected double itemLongitude;

    public Item(String itemId, double itemLatitude, double itemLongitude) {
        this.itemId = itemId;
        this.itemLatitude = itemLatitude;
        this.itemLongitude = itemLongitude;
    }

    public String getItemId() {
        return itemId;
    }

    public double getItemLatitude() {
        return itemLatitude;
    }

    public double getItemLongitude() {
        return itemLongitude;
    }

}
