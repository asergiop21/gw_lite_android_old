package com.gwlite.adapters;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;

import com.gwlite.R;
import com.gwlite.adapters.viewHolders.CheckboxViewHolder;
import com.gwlite.adapters.viewHolders.DateViewHolder;
import com.gwlite.adapters.viewHolders.EditTextViewHolder;
import com.gwlite.adapters.viewHolders.PhotoViewHolder;
import com.gwlite.adapters.viewHolders.SpinnerViewHolder;
import com.gwlite.entities.Field;
import com.gwlite.entities.fields.ListItem;
import com.gwlite.util.CropImage;
import com.gwlite.util.ImagePicker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class FieldsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int EDIT_TEXT = 1, SPINNER = 2, DATE = 3, CHECK = 4, EDIT_TEXT_NUMERIC = 5, PHOTO = 6;

    private static int IMAGE_PICKER = 1;
    private static int UPDATE_DATA = 2;
    public static Bitmap mBitmapUserPhoto;

    protected ArrayList<Field> items;
    protected Context context;
    protected boolean create;

    private int mSelectedItem;

    public FieldsAdapter(Context context, ArrayList<Field> items, boolean create) {
        this.context = context;
        this.items = items;
        this.create = create;
    }

    public FieldsAdapter() {

    }

    @Override
    public int getItemCount() {
        return this.items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getType();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        switch (viewType) {
            case EDIT_TEXT:
            case EDIT_TEXT_NUMERIC:
                View v1 = inflater.inflate(R.layout.list_item_edit_text, viewGroup, false);
                viewHolder = new EditTextViewHolder(v1);
                break;
            case SPINNER:
                View v2 = inflater.inflate(R.layout.list_item_spinner, viewGroup, false);
                viewHolder = new SpinnerViewHolder(v2);
                break;
            case DATE:
                View v3 = inflater.inflate(R.layout.list_item_date, viewGroup, false);
                viewHolder = new DateViewHolder(v3);
                break;
            case CHECK:
                View v4 = inflater.inflate(R.layout.list_item_checkbox, viewGroup, false);
                viewHolder = new CheckboxViewHolder(v4);
                break;
            case PHOTO:
                View v6 = inflater.inflate(R.layout.list_item_photo, viewGroup, false);
                viewHolder = new PhotoViewHolder(v6);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder viewHolder, final int position) {

        final Field field = items.get(position);

        switch (viewHolder.getItemViewType()) {

            case EDIT_TEXT:
            case EDIT_TEXT_NUMERIC:
                final EditTextViewHolder editText = (EditTextViewHolder) viewHolder;

                editText.getTextInputLayout().setHint(field.getName());
                editText.getEditText().setText(field.getText().getValue());

                editText.getEditText().addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        Field current = items.get(viewHolder.getAdapterPosition());

                        String text = editText.getEditText().getText().toString();
                        String regex = current.getText().getRegex();
                        if (!text.equals("")) {
                            if (!regex.equals("")) {
                                if (!text.matches(regex)) {
                                    editText.getTextInputLayout().setError("No válido");
                                } else
                                    editText.getTextInputLayout().setError(null);
                            } else
                                editText.getTextInputLayout().setError(null);
                        } else
                            editText.getTextInputLayout().setError(null);
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        Field current = items.get(viewHolder.getAdapterPosition());
                        current.getText().setValue(editable.toString());
                    }
                });

                switch (viewHolder.getItemViewType()) {
                    case EDIT_TEXT:
                        editText.getEditText().setInputType(InputType.TYPE_CLASS_TEXT);
                        break;
                    case EDIT_TEXT_NUMERIC:
                        editText.getEditText().setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                        break;
                }

                break;

            case SPINNER:
                SpinnerViewHolder spinner = (SpinnerViewHolder) viewHolder;

                spinner.getTitleTextView().setText(field.getName());

                ArrayList<String> itemsArray = new ArrayList<>();
                for (ListItem listItem : field.getItems().getItems())
                    itemsArray.add(listItem.getName());

                ArrayAdapter<String> adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, itemsArray);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.getSpinner().setAdapter(adapter);
                try {
                    Field current = items.get(viewHolder.getAdapterPosition());
                    for (int i = 0; i < current.getItems().getItems().size(); i++) {
                        ListItem listItem = current.getItems().getItems().get(i);
                        if (listItem.getId() == current.getItems().getValue())
                            spinner.getSpinner().setSelection(i);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                spinner.getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        Field current = items.get(viewHolder.getAdapterPosition());
                        current.getItems().setValue(current.getItems().getItems().get(i).getId());
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                break;

            case DATE:
                final DateViewHolder date = (DateViewHolder) viewHolder;

                date.getTitleTextView().setText(field.getName());
                if (field.getDateItem().getValue() != 0) {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    Date dateTime = new Date(field.getDateItem().getValue());
                    String stringTime = dateFormat.format(dateTime);
                    date.getDateTextView().setText(stringTime);
                } else
                    date.getDateTextView().setText("");

                date.getDateTextView().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final Calendar c = Calendar.getInstance();
                        int year = c.get(Calendar.YEAR);
                        int month = c.get(Calendar.MONTH);
                        int day = c.get(Calendar.DAY_OF_MONTH);

                        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                                new DatePickerDialog.OnDateSetListener() {

                                    @Override
                                    public void onDateSet(android.widget.DatePicker view, int year,
                                                          int monthOfYear, int dayOfMonth) {
                                        int monthDiff = monthOfYear + 1;
                                        String dtStart = dayOfMonth + "/" + monthDiff + "/" + year;
                                        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                                        try {
                                            Date dateTime = format.parse(dtStart);
                                            Field current = items.get(viewHolder.getAdapterPosition());
                                            current.getDateItem().setValue(dateTime.getTime());
                                            date.getDateTextView().setText(dtStart);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }, year, month, day);
                        datePickerDialog.show();
                    }
                });
                break;

            case CHECK:
                CheckboxViewHolder checkbox = (CheckboxViewHolder) viewHolder;

                checkbox.getCheckBox().setText(field.getName());
                checkbox.getCheckBox().setChecked(field.getCheck().getValue());

                checkbox.getCheckBox().setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        Field current = items.get(viewHolder.getAdapterPosition());
                        current.getCheck().setValue(b);
                    }
                });
                break;

            case PHOTO:
                PhotoViewHolder photo = (PhotoViewHolder) viewHolder;

                photo.getTitleTextView().setText(field.getName());
                String photoBase = field.getPhoto().getValue();
                if (photoBase != null && !photoBase.equals("")) {
                    byte[] imageAsBytes = Base64.decode(photoBase.getBytes(), Base64.DEFAULT);
                    if (imageAsBytes != null)
                        photo.getPhotoImageView().setImageBitmap(BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));
                } else
                    photo.getPhotoImageView().setImageResource(R.drawable.photo);

                photo.getPhotoImageView().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mSelectedItem = viewHolder.getAdapterPosition();
                        Intent chooseImageIntent = ImagePicker.getPickImageIntent(context);
                        ((Activity) context).startActivityForResult(chooseImageIntent, IMAGE_PICKER);
                    }
                });
                break;
        }

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == IMAGE_PICKER) {
                try {
                    mBitmapUserPhoto = ImagePicker.getImageFromResult(context, resultCode, data);
                    Intent i = new Intent(context, CropImage.class);
                    ((Activity) context).startActivityForResult(i, UPDATE_DATA);
                } catch (Exception ignored) {
                }
            }

            if (requestCode == UPDATE_DATA) {
                try {
                    if (data != null) {
                        items.get(mSelectedItem).getPhoto().setValue(data.getStringExtra("photo"));
                        notifyDataSetChanged();
                    }
                } catch (Exception ignored) {
                }
            }

        }
    }

}