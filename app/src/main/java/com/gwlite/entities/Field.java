package com.gwlite.entities;

import com.gwlite.entities.fields.Check;
import com.gwlite.entities.fields.DateItem;
import com.gwlite.entities.fields.Items;
import com.gwlite.entities.fields.Text;

import java.io.Serializable;

public class Field implements Serializable {

    protected int type;
    protected String name;
    protected boolean required;

    protected Text text;
    protected Items items;
    protected DateItem dateItem;
    protected Check check;
    protected Photo photo;

    public Field(int type, String name, boolean required, Text text) {
        this.type = type;
        this.name = name;
        this.required = required;
        this.text = text;
    }

    public Field(int type, String name, boolean required, Items items) {
        this.type = type;
        this.name = name;
        this.required = required;
        this.items = items;
    }

    public Field(int type, String name, boolean required, DateItem dateItem) {
        this.type = type;
        this.name = name;
        this.required = required;
        this.dateItem = dateItem;
    }

    public Field(int type, String name, boolean required, Check check) {
        this.type = type;
        this.name = name;
        this.required = required;
        this.check = check;
    }

    public Field(int type, String name, boolean required, Photo photo) {
        this.type = type;
        this.name = name;
        this.required = required;
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public int getType() {
        return type;
    }

    public boolean isRequired() {
        return required;
    }

    public Text getText() {
        return text;
    }

    public Items getItems() {
        return items;
    }

    public DateItem getDateItem() {
        return dateItem;
    }

    public Check getCheck() {
        return check;
    }

    public Photo getPhoto() {
        return photo;
    }
}
