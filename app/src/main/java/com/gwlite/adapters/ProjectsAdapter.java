package com.gwlite.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gwlite.R;
import com.gwlite.entities.Project;

import java.util.ArrayList;

public class ProjectsAdapter extends BaseAdapter {

    protected ArrayList<Project> items, itemsCopy;
    protected Context context;

    public ProjectsAdapter(Context context, ArrayList<Project> items) {
        this.context = context;
        this.items = items;

        itemsCopy = new ArrayList<>();
        itemsCopy.addAll(items);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return items.get(i).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inf.inflate(R.layout.list_item_project, null);

            viewHolder = new ViewHolder();
            viewHolder.nameTextView = convertView.findViewById(R.id.nameTextView);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Project project = items.get(position);

        viewHolder.nameTextView.setText(project.getProjectName());

        return convertView;
    }

    public class ViewHolder {
        TextView nameTextView;
    }

    public void filter(String text) {
        items.clear();
        if (text.isEmpty()) {
            items.addAll(itemsCopy);
        } else {
            text = text.toLowerCase();
            for (Project item : itemsCopy) {
                if (item.getProjectName().toLowerCase().contains(text)) {
                    items.add(item);
                }
            }
        }
        notifyDataSetChanged();
    }

}