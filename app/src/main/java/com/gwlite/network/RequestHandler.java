package com.gwlite.network;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Po on 11/06/2015.
 */
public class RequestHandler extends JsonObjectRequest {

    public RequestHandler(int method, String url, Map<String, Object> params,
                          Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(method, url, new JSONObject(params), listener, errorListener);

    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> headers = new HashMap<>();
        // do not add anything here
        return headers;
    }

}