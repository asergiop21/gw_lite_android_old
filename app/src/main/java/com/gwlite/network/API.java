package com.gwlite.network;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.gwlite.AppController;
import com.gwlite.Parameters;
import com.gwlite.SharedPreferencesManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.gwlite.Parameters.EMAIL;
import static com.gwlite.Parameters.TOKEN;

public class API {

    private static final String MASTER_URL = "http://public.api.geoworks.com.ar/api/v1";

    private static final String USER = "/sessions";
    private static final String PROJECTS = "/project_types";
    private static final String PROJECT_ITEMS = "/projects?project_type_id=";
    private static final String PROJECT_FIELDS = "/project_fields?project_type_id=";
    private static final String CREATE_ITEM = "/projects";

    private final int TIMEOUT_NORMAL = 60000;
    private final int NO_RETRIES = 0;

    private void addToQueue(RequestHandler jsonObjReq) {
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }

    public void login(String userEmail, String userPassword, Response.Listener<JSONObject> onResponse, Response.ErrorListener onErrorResponse) {

        Map<String, Object> params = new HashMap<>();
        params.put(Parameters.USER_EMAIL, userEmail);
        params.put(Parameters.USER_PASSWORD, userPassword);

        RequestHandler jsonObjReq = new RequestHandler(Request.Method.POST, MASTER_URL + USER, params, onResponse, onErrorResponse);

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                TIMEOUT_NORMAL, NO_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        addToQueue(jsonObjReq);
    }

    public void getProjects(Response.Listener<JSONObject> onResponse, Response.ErrorListener onErrorResponse) {

        Map<String, Object> params = new HashMap<>();

        RequestHandler jsonObjReq = new RequestHandler(Request.Method.GET,
                SharedPreferencesManager.getInstance().getAppInstance().getInstanceUrl() + PROJECTS, params, onResponse, onErrorResponse) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = super.getHeaders();
                if (params == null) params = new HashMap<>();
                params.put(EMAIL, SharedPreferencesManager.getInstance().getUser().getUserEmail());
                params.put(TOKEN, SharedPreferencesManager.getInstance().getUser().getUserId());
                return params;
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                TIMEOUT_NORMAL, NO_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        addToQueue(jsonObjReq);
    }

    public void getProjectItems(String projectId, Response.Listener<JSONObject> onResponse, Response.ErrorListener onErrorResponse) {

        Map<String, Object> params = new HashMap<>();

        RequestHandler jsonObjReq = new RequestHandler(Request.Method.GET,
                SharedPreferencesManager.getInstance().getAppInstance().getInstanceUrl() + PROJECT_ITEMS + projectId,
                params, onResponse, onErrorResponse) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = super.getHeaders();
                if (params == null) params = new HashMap<>();
                params.put(EMAIL, SharedPreferencesManager.getInstance().getUser().getUserEmail());
                params.put(TOKEN, SharedPreferencesManager.getInstance().getUser().getUserId());
                return params;
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                TIMEOUT_NORMAL, NO_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        addToQueue(jsonObjReq);
    }

    public void getProjectFields(String projectId, Response.Listener<JSONObject> onResponse, Response.ErrorListener onErrorResponse) {

        Map<String, Object> params = new HashMap<>();

        RequestHandler jsonObjReq = new RequestHandler(Request.Method.GET,
                SharedPreferencesManager.getInstance().getAppInstance().getInstanceUrl() + PROJECT_FIELDS + projectId,
                params, onResponse, onErrorResponse) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = super.getHeaders();
                if (params == null) params = new HashMap<>();
                params.put(EMAIL, SharedPreferencesManager.getInstance().getUser().getUserEmail());
                params.put(TOKEN, SharedPreferencesManager.getInstance().getUser().getUserId());
                return params;
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                TIMEOUT_NORMAL, NO_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        addToQueue(jsonObjReq);
    }

    public void getProjectFieldsUpdate(String itemId, Response.Listener<JSONObject> onResponse, Response.ErrorListener onErrorResponse) {

        Map<String, Object> params = new HashMap<>();

        RequestHandler jsonObjReq = new RequestHandler(Request.Method.GET,
                SharedPreferencesManager.getInstance().getAppInstance().getInstanceUrl() + CREATE_ITEM + "/" + itemId,
                params, onResponse, onErrorResponse) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = super.getHeaders();
                if (params == null) params = new HashMap<>();
                params.put(EMAIL, SharedPreferencesManager.getInstance().getUser().getUserEmail());
                params.put(TOKEN, SharedPreferencesManager.getInstance().getUser().getUserId());
                return params;
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                TIMEOUT_NORMAL, NO_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        addToQueue(jsonObjReq);
    }

    public void createItem(JSONArray arrayItems, Response.Listener<JSONObject> onResponse, Response.ErrorListener onErrorResponse) {

        Map<String, Object> params = new HashMap<>();
        params.put("data", arrayItems);

        System.out.println(params.toString());
        RequestHandler jsonObjReq = new RequestHandler(Request.Method.POST,
                SharedPreferencesManager.getInstance().getAppInstance().getInstanceUrl() + CREATE_ITEM, params,
                onResponse, onErrorResponse) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = super.getHeaders();
                if (params == null) params = new HashMap<>();
                params.put(EMAIL, SharedPreferencesManager.getInstance().getUser().getUserEmail());
                params.put(TOKEN, SharedPreferencesManager.getInstance().getUser().getUserId());
                return params;
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                TIMEOUT_NORMAL, NO_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        addToQueue(jsonObjReq);
    }

    public void updateItem(String projectId, String itemId, JSONObject properties, Response.Listener<JSONObject> onResponse,
                           Response.ErrorListener onErrorResponse) {

        Map<String, Object> params = new HashMap<>();

        JSONObject project = new JSONObject();
        try {
            project.put(Parameters.PROJECT_TYPE_ID, projectId);
            project.put(Parameters.PROPERTIES, properties);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        params.put("project", project);

        RequestHandler jsonObjReq = new RequestHandler(Request.Method.PUT,
                SharedPreferencesManager.getInstance().getAppInstance().getInstanceUrl() + CREATE_ITEM + "/" + itemId,
                params, onResponse, onErrorResponse) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = super.getHeaders();
                if (params == null) params = new HashMap<>();
                params.put(EMAIL, SharedPreferencesManager.getInstance().getUser().getUserEmail());
                params.put(TOKEN, SharedPreferencesManager.getInstance().getUser().getUserId());
                return params;
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                TIMEOUT_NORMAL, NO_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        addToQueue(jsonObjReq);
    }
}