package com.gwlite.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.gwlite.Parameters;
import com.gwlite.R;
import com.gwlite.adapters.ProjectsAdapter;
import com.gwlite.entities.Project;
import com.gwlite.network.API;
import com.gwlite.util.GeoWorksParser;
import com.gwlite.util.SimpleDividerItemDecoration;
import com.gwlite.util.SyncServices;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class Projects extends AppCompatActivity {

    private Context mContext = this;
    private ActionBar mActionBar;
    private ProgressDialog mLoading;

    private EditText mProjectEditText;
    private RecyclerView mProjectsRecyclerView;
    private RelativeLayout mErrorRelativeLayout;

    private ProjectsAdapter mProjectsAdapter;
    private ArrayList<Project> mProjects;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projects);

        mActionBar = getSupportActionBar();
        setupActionBar();

        mProjectEditText = findViewById(R.id.projectEditText);
        mProjectsRecyclerView = findViewById(R.id.projectsRecyclerView);
        mErrorRelativeLayout = findViewById(R.id.errorRelativeLayout);

        mProjectsRecyclerView.setHasFixedSize(true);
        mProjectsRecyclerView.setLayoutManager(new LinearLayoutManager(mContext,
                LinearLayoutManager.VERTICAL, false));
        mProjectsRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getResources()));

        cleanProjects();

        mErrorRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cleanProjects();
            }
        });
    }

    private void cleanProjects() {
        mErrorRelativeLayout.setVisibility(RelativeLayout.GONE);
        if (mProjects != null)
            mProjects.clear();
        if (mProjectsRecyclerView.getAdapter() != null) {
            mProjectsRecyclerView.getAdapter().notifyDataSetChanged();
            mProjectsAdapter = null;
        }
        updateProjects();
    }

    private void updateProjects() {

        showProgressDialog();

        new API().getProjects(new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject result) {
                try {
                    JSONArray data = result.getJSONArray(Parameters.DATA);
                    readProjects(data);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                resetProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error.toString());
                resetProgressDialog();
                showErrorAnimation();
            }
        });
    }

    private void showErrorAnimation() {
        mErrorRelativeLayout.setVisibility(RelativeLayout.VISIBLE);
    }

    private void readProjects(JSONArray data) {
        try {
            mProjects = new ArrayList<>();

            for (int i = 0; i < data.length(); i++) {
                Project project = GeoWorksParser.parseProject(data.getJSONObject(i));
                if (project != null)
                    mProjects.add(project);
            }

            mProjectsAdapter = new ProjectsAdapter(mContext, mProjects);
            ///mProjectsRecyclerView.setAdapter(mProjectsAdapter);

           /* mProjectsAdapter.setOnItemClickListener(new ProjectsAdapter.ClickListener() {
                @Override
                public void onItemClick(int position, View v) {
                    Project project = mProjects.get(position);

                    Intent projectView = new Intent(mContext, ProjectView.class);
                    projectView.putExtra(Parameters.BUNDLE_PROJECT_ID, project.getProjectId());
                    projectView.putExtra(Parameters.BUNDLE_PROJECT_NAME, project.getProjectName());
                    startActivity(projectView);
                }
            });

            mProjectEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    mProjectsAdapter.filter(charSequence.toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });*/

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupActionBar();
    }

    private void setupActionBar() {
        getSupportActionBar().setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));

        mActionBar.setDisplayShowHomeEnabled(false);
        //displaying custom ActionBar
        View mActionBarView = getLayoutInflater().inflate(R.layout.custom_action_bar, null);
        mActionBar.setCustomView(mActionBarView);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        mActionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.primary_color)));
        TextView titleTextView = mActionBarView.findViewById(R.id.titleTextView);
        ImageButton syncImageButton = mActionBarView.findViewById(R.id.action2ImageButton);

        titleTextView.setText("Proyectos");

        if (!getSync()) {
            syncImageButton.setVisibility(ImageButton.VISIBLE);
            syncImageButton.setImageResource(R.drawable.sync_off);
        } else
            syncImageButton.setVisibility(ImageButton.GONE);

        syncImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                syncItems();
            }
        });
    }

    private boolean getSync() {
        int items = new SyncServices(mContext).getAmountUnsyncedItems();
        if (items > 0)
            return false;

        return true;
    }

    private void syncItems() {

        JSONArray arrayItems = new SyncServices(mContext).getUnsyncedItems();

        new API().createItem(arrayItems, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                resetProgressDialog();
                showToast("Registros sincronizados correctamente");
                new SyncServices(mContext).deleteSyncedItems();
                setupActionBar();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                resetProgressDialog();
                System.out.println(error.toString());
                showToast(error.toString());
            }
        });
    }

    private void showToast(String message) {
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
    }

    private void resetProgressDialog() {
        if ((mLoading != null)) {
            if (mLoading.isShowing()) {
                mLoading.dismiss();
            }
        }
    }

    private void showProgressDialog() {
        mLoading = new ProgressDialog(mContext);
        mLoading.show();
        mLoading.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mLoading.setContentView(R.layout.custom_loading);
        mLoading.setCancelable(false);
        mLoading.setCanceledOnTouchOutside(false);
    }

}