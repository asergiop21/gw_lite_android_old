package com.gwlite.entities.fields;

import java.io.Serializable;

public class DateItem implements Serializable {

    protected long value;

    public DateItem() {

    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }

}
