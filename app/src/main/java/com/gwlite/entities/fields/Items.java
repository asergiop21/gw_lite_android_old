package com.gwlite.entities.fields;

import java.io.Serializable;
import java.util.ArrayList;

public class Items implements Serializable {

    protected ArrayList<ListItem> items;
    protected int value;

    public Items(ArrayList<ListItem> items) {
        this.items = items;
    }

    public ArrayList<ListItem> getItems() {
        return items;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
