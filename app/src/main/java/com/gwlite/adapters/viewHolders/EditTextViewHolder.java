package com.gwlite.adapters.viewHolders;

import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;

import com.gwlite.R;

public class EditTextViewHolder extends RecyclerView.ViewHolder {

    private TextInputLayout textInputLayout;
    private EditText editText;

    public EditTextViewHolder(View v) {
        super(v);
        textInputLayout = v.findViewById(R.id.textInputLayout);
        editText = v.findViewById(R.id.editText);
    }

    public TextInputLayout getTextInputLayout() {
        return textInputLayout;
    }

    public EditText getEditText() {
        return editText;
    }
}