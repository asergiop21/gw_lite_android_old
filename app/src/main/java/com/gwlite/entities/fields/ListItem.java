package com.gwlite.entities.fields;

import java.io.Serializable;

public class ListItem implements Serializable {

    protected int id;
    protected String name;

    public ListItem(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

}
