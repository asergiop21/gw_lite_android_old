package com.gwlite.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.gwlite.R;
import com.here.android.mpa.search.AutoSuggest;
import com.here.android.mpa.search.AutoSuggestPlace;
import com.here.android.mpa.search.ErrorCode;
import com.here.android.mpa.search.ResultListener;
import com.here.android.mpa.search.TextAutoSuggestionRequest;

import java.util.ArrayList;
import java.util.List;

public class PlacesAutoCompleteAdapter extends ArrayAdapter<AutoSuggest> implements Filterable {

    private Context context;
    private ArrayList<AutoSuggest> resultList;

    public PlacesAutoCompleteAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        this.context = context;
    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Override
    public AutoSuggest getItem(int index) {
        return resultList.get(index);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.item_search, parent, false);

        AutoSuggest autoSuggest = resultList.get(position);
        AutoSuggestPlace autoSuggestPlace = (AutoSuggestPlace) autoSuggest;

        TextView placeTextView = view.findViewById(R.id.placeTextView);
        placeTextView.setText(autoSuggest.getTitle() + ", " + autoSuggestPlace.getVicinity());

        return view;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            protected FilterResults performFiltering(final CharSequence constraint) {
                final FilterResults filterResults = new FilterResults();

                if (constraint != null) {

                    TextAutoSuggestionRequest request;
                    request = new TextAutoSuggestionRequest(constraint.toString());

                    if (request.execute(new ResultListener<List<AutoSuggest>>() {
                        @Override
                        public void onCompleted(List<AutoSuggest> autoSuggests, ErrorCode errorCode) {
                            try {

                                ArrayList<AutoSuggest> results = new ArrayList<>();

                                if (autoSuggests != null) {

                                    int count = 0;
                                    for (int i = 0; i < autoSuggests.size(); i++) {
                                        if (count < 5) {
                                            AutoSuggest autoSuggest = autoSuggests.get(i);

                                            if (autoSuggest instanceof AutoSuggestPlace) {

                                                AutoSuggestPlace autoSuggestPlace = (AutoSuggestPlace) autoSuggest;

                                                if (autoSuggestPlace.getVicinity() != null) {
                                                    results.add(autoSuggest);
                                                    count++;
                                                }
                                            }
                                        } else
                                            break;
                                    }

                                } else
                                    Log.e("ERROR: ", errorCode.toString());

                                resultList = results;

                                // Assign the data to the FilterResults
                                filterResults.values = resultList;
                                filterResults.count = resultList.size();

                                publishResults(constraint, filterResults);

                            } catch (Exception e) {
                                Log.e("ERROR: ", e.getMessage());
                            }
                        }
                    }) != ErrorCode.NONE) {
                        Log.d("PlacesAutoCompleAdapter", "Error on TextAutoSuggestionRequest");
                    }

                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint,
                                          FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };

        return filter;
    }

}