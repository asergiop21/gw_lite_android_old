package com.gwlite.adapters.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.gwlite.R;

public class PhotoViewHolder extends RecyclerView.ViewHolder {

    private TextView titleTextView;
    private ImageView photoImageView;

    public PhotoViewHolder(View v) {
        super(v);
        titleTextView = v.findViewById(R.id.titleTextView);
        photoImageView = v.findViewById(R.id.photoImageView);
    }

    public TextView getTitleTextView() {
        return titleTextView;
    }

    public ImageView getPhotoImageView() {
        return photoImageView;
    }
}