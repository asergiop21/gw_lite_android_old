package com.gwlite.entities;

public class Photo {

    protected String value;

    public Photo() {
    }

    public Photo(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
