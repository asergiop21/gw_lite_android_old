package com.gwlite;

public class Parameters {

    public static final String DATA = "data";


    public static final String EMAIL = "X-User-Email";
    public static final String TOKEN = "X-User-Token";

    public static final String USER_ID = "authentication_token";
    public static final String USER_EMAIL = "email";
    public static final String USER_PASSWORD = "password";
    public static final String PROJECT_TYPE_ID = "project_type_id";

    public static final String ID = "id";
    public static final String ATTRIBUTES = "attributes";
    public static final String PROPERTIES = "properties";
    public static final String PHOTOS = "photos_attributes";
    public static final String THE_GEOM = "the_geom";
    public static final String NAME = "name";
    public static final String FIELD_TYPE = "field_type_id";
    public static final String REGEX = "regexp";

    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String FIELDS = "fields";

    public static final String BUNDLE_PROJECT_ID = "projectId";
    public static final String BUNDLE_PROJECT_NAME = "projectName";
    public static final String BUNDLE_ITEM_ID = "itemId";
}
