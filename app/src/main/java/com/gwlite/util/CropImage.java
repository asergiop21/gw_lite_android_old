package com.gwlite.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.edmodo.cropper.CropImageView;
import com.gwlite.R;
import com.gwlite.adapters.FieldsAdapter;

import java.io.ByteArrayOutputStream;

public class CropImage extends AppCompatActivity {

    private Context mContext = this;

    private CropImageView mCropImageView;
    private RelativeLayout mRotateRelativeLayout;
    private Button mCancelButton, mAcceptButton;

    private Bitmap mCroppedBitmap;

    public static Bitmap scaleDown(Bitmap realImage, float maxImageSize,
                                   boolean filter) {
        float ratio = Math.min(maxImageSize / realImage.getWidth(), maxImageSize / realImage.getHeight());
        int width = Math.round(ratio * realImage.getWidth());
        int height = Math.round(ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width, height, filter);

        return newBitmap;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop_image);

        mCropImageView = findViewById(R.id.cropImageView);
        mRotateRelativeLayout = findViewById(R.id.rotateRelativeLayout);
        mCancelButton = findViewById(R.id.cancelButton);
        mAcceptButton = findViewById(R.id.acceptButton);

        mCropImageView.setImageBitmap(FieldsAdapter.mBitmapUserPhoto);
        mCropImageView.setFixedAspectRatio(true);

        mRotateRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCropImageView.rotateImage(90);
            }
        });

        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Activity) mContext).finish();
            }
        });

        mAcceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    mCroppedBitmap = mCropImageView.getCroppedImage();
                    mCroppedBitmap = scaleDown(mCroppedBitmap, 500, true);
                    updatePhoto();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void updatePhoto() {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        mCroppedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteArray = stream.toByteArray();

        final String vectorBytes = Base64.encodeToString(byteArray, Base64.CRLF);
        Intent intent = new Intent();
        intent.putExtra("photo", vectorBytes);
        setResult(Activity.RESULT_OK, intent);
        ((Activity) mContext).finish();
    }

}