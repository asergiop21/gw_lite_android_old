package com.gwlite.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gwlite.R;
import com.gwlite.entities.Field;
import com.gwlite.entities.FieldParent;
import com.gwlite.entities.Photo;

import java.util.ArrayList;

public class FieldsParentAdapter extends RecyclerView.Adapter<FieldsParentAdapter.ViewHolder> {

    private Context context;
    private ArrayList<FieldParent> items;
    private boolean create;

    public FieldsParentAdapter(Context context, ArrayList<FieldParent> items, boolean create) {
        this.context = context;
        this.items = items;
        this.create = create;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_field_parent, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        FieldParent parent = items.get(position);

        holder.nameTextView.setText(parent.getName());

        holder.fieldsRecyclerView.setHasFixedSize(true);
        holder.fieldsRecyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        holder.fieldsRecyclerView.setAdapter(new FieldsAdapter(context, parent.getFields(), create));

        holder.photoLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FieldParent current = items.get(holder.getLayoutPosition());
                current.getFields().add(new Field(6, "Foto " + getPhotoCounter(current.getFields()), true, new Photo()));
                notifyDataSetChanged();
            }
        });

    }

    private int getPhotoCounter(ArrayList<Field> fields) {
        int counter = 1;
        for (Field field : fields) {
            switch (field.getType()) {
                case 6:
                    counter++;
                    break;
            }
        }
        return counter;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTextView;
        public LinearLayout photoLinearLayout;
        public RecyclerView fieldsRecyclerView;

        private ViewHolder(View itemView) {
            super(itemView);
            nameTextView = itemView.findViewById(R.id.titleTextView);
            photoLinearLayout = itemView.findViewById(R.id.photoLinearLayout);
            fieldsRecyclerView = itemView.findViewById(R.id.fieldsRecyclerView);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        new FieldsAdapter().onActivityResult(requestCode, resultCode, data);
    }

}