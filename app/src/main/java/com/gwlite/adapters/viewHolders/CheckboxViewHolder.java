package com.gwlite.adapters.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;

import com.gwlite.R;

public class CheckboxViewHolder extends RecyclerView.ViewHolder {

    private CheckBox checkBox;

    public CheckboxViewHolder(View v) {
        super(v);
        checkBox = v.findViewById(R.id.checkbox);
    }

    public CheckBox getCheckBox() {
        return checkBox;
    }
}