package com.gwlite.entities;

import java.io.Serializable;
import java.util.ArrayList;

public class FieldParent implements Serializable {

    protected String name;
    protected ArrayList<Field> fields;

    public FieldParent(String name, ArrayList<Field> fields) {
        this.name = name;
        this.fields = fields;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Field> getFields() {
        return fields;
    }
}
